/**
 * 
 *  fichier qui va contenir toutes les  prototypes des fonctions de connection TCP  du coté serveur
 *  
 **/

#ifndef TCPSRV_H
#define TCPSRV_H


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <netdb.h>
#include <unistd.h>
#include <iostream> 
#include <cstdlib>

#include <stdarg.h>
#include <string.h>
 
#define  PORT_NUM   "1055" 
#define  QLEN 32                         // nombre maximal des connexion en attente au serveur



extern int        errno;                // gestion des erreurs

int erreur_exit(const char *format, ...);

int serveur_tcp(const char *service, int qlen);                                    //méthode de connection au serveur
int connexion_tcp(const char *host, const char *service);                           // méthode de connection du client
int connexion_socket(const char *service, const char *transport, int qlen );
int connexion_socket(const char *host, const char *service, const char *transport );

#endif