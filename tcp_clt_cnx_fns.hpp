/**
 * 
 *  fichier qui va contenir toutes les  prototypes des fonctions de connection TCP  du coté client
 *  le fichier client n est pas partagé avec celui du serveur
 *  afin de permettre un developpement séparé au cas ou on utiliserait
 *  un autre language pour coder le client /qt par ex ou c#
 *  
 **/

#ifndef TCPCLT_H
#define TCPCLT_H


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <netdb.h>
#include <unistd.h>
#include <iostream> 
#include <cstdlib>

#include <stdarg.h>
#include <string.h>

// defines de la partie du programme qui ne connecte simplement au serveur tictactoe main
// pour obtenir la liste des joueurs
#define PORT_NUM    "1055"              // Numero de port au serveur principale tictactoe(liste)
#define IP_ADDR     "127.0.0.1"         // IP local du client qui se connecte au serveur


// defines de la partie du programme qui se comporte comme client ou serveur
// en connection avec un autre client ou serveur pendant le plein jeu
#define  QLEN 1                         // nombre maximal des connexion en attente que le client
                                        //  serveur temp peut prendre



extern int        errno;                // gestion des erreurs

int erreur_exit(const char *format, ...);

int serveur_tcp(const char *service, int qlen);                                    //méthode de connection au serveur
int connexion_tcp(const char *host, const char *service);                           // méthode de connection du client
int connexion_socket(const char *service, const char *transport, int qlen );
int connexion_socket(const char *host, const char *service, const char *transport );

#endif