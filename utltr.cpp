// fichier source des fonctions utilitaires
#include "utltr.hpp"
/*  
 *  erreur exit - affiche un message d'erreur and quitte le programme
 * 
 * */
int erreur_exit(const char *format, ...)
{
  va_list args;

  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  exit(1);
}