/**
 *  fonction qui va contenir la fonction de gestion
 *  d'erreur : fichier d'utilitaires
 **/

#ifndef UTLTR_H
#define UTLTR_H

#include <cstdio>
#include <cstdlib>
#include <cstdarg>

extern int        errno;                // gestion des erreurs

int erreur_exit(const char *format, ...);

//testing de mon nouveau pc


#endif