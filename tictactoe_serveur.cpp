/**
 * fichier principale(main) du serveur tcp
 * le serveur va recevoir les connections des joueurs et va en informer les tout le monde
 * 
*/
#include <sys/socket.h>
#include <sys/signal.h>
#include <pthread.h>
#include <iostream> 
#include <vector>

#include "tcp_srv_cnx_fns.hpp"
#include "tictactoe_serveur_fns.hpp"


using namespace std;

// info sur le client lors de la première connection
struct client_t
{
  string client_ip;
  int    client_socket;
};

int echange_avec_client(void *);                    // illustrer échange entre client et serveur
static void *sig_thread(void *);

pthread_mutex_t   table_player_mutex;               // le mutex sur la liste des joueurs
vector<player_t>  table_player;                     // variable qui va contenir les noms d'utilisateurs au serveur

//#define  BUFF_SIZE  4096
char buffer[TAILLE_BUF];

// Main 
int main(void)
{
    struct sockaddr_in  fsin;                       // l'adresse provenant du client              
    socklen_t  t_addr;                              // t_addr : va contenir la taille adresse du client

    int client_s, serveur_s;                        //  socket client

    serveur_s = serveur_tcp(PORT_NUM, QLEN);

    pthread_t     th_main;
    pthread_attr_t  th_ta;

    sigset_t set;
           int s;

    (void) pthread_attr_init(&th_ta);
    (void) pthread_attr_setdetachstate(&th_ta, PTHREAD_CREATE_DETACHED);
    (void) pthread_mutex_init(&table_player_mutex, 0);

    // ensemble gestion des signaux
    sigemptyset(&set);
    sigaddset(&set, SIGQUIT);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGUSR1);
    s = pthread_sigmask(SIG_BLOCK, &set, NULL);
    if (s != 0)
        erreur_exit("pthread_sigmask: %s\n", strerror(errno));

    if(pthread_create(&th_main, &th_ta, &sig_thread, (void *) &set) < 0)
    {
        erreur_exit("pthread_create(th_signals): %s\n", strerror(errno));
    }
    // 
    while(1)
    {
        t_addr = sizeof(fsin);                              // taille de l'adresse
        client_s = accept(serveur_s, (struct sockaddr *)&fsin, &t_addr);
  
        if( client_s < 0){
            if(errno == EINTR)   // permet d'intercepter le signal au cas ou le père n'est pas au courant de la terminaison du fils
                continue;
            erreur_exit("Acceptation de la connexion échoué %s", strerror(errno));
        }
        client_t info_client;
        info_client.client_ip = inet_ntoa(fsin.sin_addr);
        info_client.client_socket = client_s;
        //on crée le thread qui va gérer les connections clientes
        if(pthread_create(&th_main, &th_ta, (void * (*)(void *))echange_avec_client, (void*)&info_client) < 0)
        {
            erreur_exit("pthread_create(th_main): %s\n", strerror(errno));
        }
    }
    //fin
    return 0;
}
// méthode d'interaction entre le client et le serveur
// le client et le serveur communique par des commandes de format CMD:OPTION1:OPTION2:OPTION_N
// ces commandes sont en chaine de caractères, lorsqu'ils sont recu par recv, ou envoyé par send
// il existe une méthode(split_cmd) qui va les diviser en partie afin de prévoir un résultat en fonction de la commande donnée
// la méthode recoit en paramètre une structure qui contient l'adresse ip du client et sont socket
// la méthode retourne zéro si toutes les opérations se sont déroulées comme prévues
int echange_avec_client(void* s)
{
    client_t c_info = *(client_t *)s;
    int socket =  c_info.client_socket;
    int n(0);
    //char buffer[]

    string cmd, option, pseudo, port, cmd_out; // variable interne à utiliser
    vector<string> t_cmd; // recoit les commandes partie par partie
    
    while((n = recv(socket, buffer, sizeof(buffer),0)) > 0)
    {
      if(n < 0){
          erreur_exit("recv: %s\n", strerror(errno));
      }
      cmd = buffer;             // on copie simplement l'interieur du buffer(char[]) dans la variable commande(string)
      t_cmd = split_cmd(cmd);   // on décompose la commande en option

      if(t_cmd[0] == "CUSR")    // CUSR : CREATE USER : commande pour dire qu'on crée un nouvel utilisation(table des joueur) 
      {                         // OPTION 1 : pseudo du client à créer, OPTION 2 : le port local du client à créer 
        pseudo = t_cmd[1];      // contiendra le pseudo envoyé par le client
        port   = t_cmd[2];      // va contenir le port local du prochain serveur c - c
        
        (void) pthread_mutex_lock(&table_player_mutex);
        set_player(table_player, socket, pseudo, port, c_info.client_ip);  // on enregistre les information du nouvel utilisateur
        (void) pthread_mutex_unlock(&table_player_mutex);

        cmd_out.append("NUSR:");
        cmd_out.append(pseudo);
        cmd_out.append(":");
        cmd_out.append(c_info.client_ip);
        cmd_out.append(":");
        cmd_out.append(port);
        cmd_out.append(":");
        cmd_out.append("1");
        //cmd_out.append(to_string(socket));
        //message_players(cmd_out);   // ensuite on envoit un message à tous le monde pour les mettre au courant
        (void) pthread_mutex_lock(&table_player_mutex);
        message_players(table_player, cmd_out);
        (void) pthread_mutex_unlock(&table_player_mutex);
      }
      if(t_cmd[0] == "DUSR")    // DUSER : DELETE USER : commande pour effacer un utilisateur qui s'est déconnecté
      {                         // en fonction du socket de communication on saura quel utilisateur à enlever de notre table player
        //player_t player = get_player_socket(socket);
        (void) pthread_mutex_lock(&table_player_mutex);
        player_t player = get_player_by_socket(table_player, socket);
        (void) pthread_mutex_unlock(&table_player_mutex);
      	cmd_out.append("DUSR:");
        cmd_out.append(player.player_pseudo);
        cmd_out.append(":");
        cmd_out.append("0");    // le user à effacer n'a plus de socket
        // on efface l'utilisateur et on envoi un message à tout le monde afin de l'enlever de leur table
        (void) pthread_mutex_lock(&table_player_mutex);
        remove_player(table_player, player);
        (void) pthread_mutex_unlock(&table_player_mutex);
        
        cout << player.player_pseudo << " s'est deconnecté..." << endl;
        //message_players(cmd_out);
        (void) pthread_mutex_lock(&table_player_mutex);
        message_players(table_player, cmd_out);
        (void) pthread_mutex_unlock(&table_player_mutex);

        (void) pthread_mutex_lock(&table_player_mutex);
        afficher_player(table_player);
        (void) pthread_mutex_unlock(&table_player_mutex);
        break;
      }
      if(t_cmd[0] == "EUSR")    // EUSER : EDIT USER : commande pour signaler un changement à l'état d'un jouer(libre ou pas)
      {                         // Intervient lorsque l'utilisateur est entrain de jouer avec un autre utilisateur
	      //player_t player = get_player_socket(socket);
        (void) pthread_mutex_lock(&table_player_mutex);
        player_t player = get_player_by_socket(table_player, socket);
        (void) pthread_mutex_unlock(&table_player_mutex);
        cmd_out.append("EUSR:");
        cmd_out.append(player.player_pseudo);
        cmd_out.append(":");
        // cmd_out.append(to_string(socket));
        cmd_out.append("0");
        // on fait une mise à jour du status du joueur
        // edit_player(player);
        (void) pthread_mutex_lock(&table_player_mutex);
        set_busy(table_player, player, false);
        (void) pthread_mutex_unlock(&table_player_mutex);
        //message_players(cmd_out);   // on envoit un message à tout le monde pour dire que tel joueur n'est plus libre
        (void) pthread_mutex_lock(&table_player_mutex);
        message_players(table_player, cmd_out);
        (void) pthread_mutex_unlock(&table_player_mutex);
      }
      if(t_cmd[0] == "BUSR")
      {
        //player_t player = get_player_socket(socket);
        (void) pthread_mutex_lock(&table_player_mutex);
        player_t player = get_player_by_socket(table_player, socket);
        (void) pthread_mutex_unlock(&table_player_mutex);
        cmd_out.append("BUSR:");
        cmd_out.append(player.player_pseudo);
        cmd_out.append(":");
        // cmd_out.append(to_string(socket));
        cmd_out.append("0");
        // on fait une mise à jour du status du joueur
        (void) pthread_mutex_lock(&table_player_mutex);
        set_busy(table_player, player, true);
        (void) pthread_mutex_unlock(&table_player_mutex);
        //message_players(cmd_out);   // on envoit un message à tout le monde pour dire que tel joueur n'est plus libre
        (void) pthread_mutex_lock(&table_player_mutex);
        message_players(table_player, cmd_out);
        (void) pthread_mutex_unlock(&table_player_mutex);
      }
      //
      cout << "Liste des joueurs connectés au serveur" << endl;
      cout << "********************************************************" << endl;
      cout << endl;
      (void) pthread_mutex_lock(&table_player_mutex);
      afficher_player(table_player);
      (void) pthread_mutex_unlock(&table_player_mutex);
      
      cout << "********************************************************" << endl;
      cmd_out = "";     // réinitialisons la variable à cause des appends qui surviennent la haut
      // on vide le buffer
      for(int i(0); i < TAILLE_BUF; i++)
        buffer[i] = '\0';

    }
    close(socket);
    return 0;
}
//méthode qui va capter un signal et l'envoyer à tous ceux qui sont connecté
static void *sig_thread(void *arg)
{
    sigset_t *set = (sigset_t *)arg;
    int s, sig;

    while (1) 
    {
        s = sigwait(set, &sig);
        if (s != 0)
            erreur_exit("sig: %s\n", strerror(errno));
        switch(sig)
        {
          case SIGINT:
          {//on envoi un message à tous le monde en leur disant que le serveur est mort
            cout << "server disconnecting..." << endl; 
            (void) pthread_mutex_lock(&table_player_mutex);
            message_players(table_player, "SDWN:0:0:0:0");
            (void) pthread_mutex_unlock(&table_player_mutex);
          }
          break;
          case SIGQUIT:
          {//on envoi un message à tous le monde en leur disant que le serveur est mort
            cout << "server disconnecting..." << endl; 
            (void) pthread_mutex_lock(&table_player_mutex);
            message_players(table_player, "SDWN:0:0:0:0");
            (void) pthread_mutex_unlock(&table_player_mutex);
          }
          break;
          case SIGUSR1:
          {//on envoi un message à tous le monde en leur disant que le serveur est mort
            cout << "server disconnecting..." << endl; 
            (void) pthread_mutex_lock(&table_player_mutex);
            message_players(table_player, "SDWN:0:0:0:0");
            (void) pthread_mutex_unlock(&table_player_mutex);
          }
          break;
          default:
          {
            cout << "Erreur survenue, le serveur doit s'arrêter..." << endl; 
            (void) pthread_mutex_lock(&table_player_mutex);
            message_players(table_player, "SDWN:0:0:0:0");
            (void) pthread_mutex_unlock(&table_player_mutex);
          }
          break;
        }
        exit(EXIT_SUCCESS);
    }
}