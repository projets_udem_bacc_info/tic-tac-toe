/**
 *  fichier qui va contenir toutes les  Fonctions d'interactions avec le serveur tictactoe
 */

#ifndef TICTACTOE_FNS_H
#define TICTACTOE_FNS_H

#include <sys/socket.h>
#include <iostream> 
#include <vector>
#include <string.h>


#define  TAILLE_BUF 4096   

using namespace std;

struct player_t
{
  string  player_pseudo;              // pseudo du client
  string  player_ip;                  // contenir adress ip du client
  int     player_port;                // contenir le port du client
  int     player_socket;              // socket du client pour communication
  bool    player_libre = true;        // dire si le joeur est libre ou pas
  bool    player_modified = false;    // dire s'il y a eu un changement d'état sur le joueur ou pas
};

player_t get_player_by_socket(vector<player_t>, int);
player_t get_player_by_name(vector<player_t>, string);
void set_player(vector<player_t>&, int, string, string, string);
void afficher_player(vector<player_t>);
void message_players(vector<player_t>,string);
void remove_player(vector<player_t>&, player_t);
void set_busy(vector<player_t>&, player_t, bool); // meme que set_player à modifier

// fonction utilitaire
vector<string> split_cmd(string buffer);
void  send_message(int, string);

#endif