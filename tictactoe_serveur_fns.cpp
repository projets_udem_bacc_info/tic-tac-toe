
#include "tictactoe_serveur_fns.hpp"

// fonction qui va nous permettre de récupérer un joueur par son socket
// la fonction recoit en paramètre la liste des joeurs et le socket
// et renvoit le joeur correspand de la liste des joueurs connecté au serveur
player_t get_player_by_socket(vector<player_t>table_player, int socket)
{
    player_t temp;
    for(int i(0); i < table_player.size(); ++i)
    {
        if(table_player.at(i).player_socket == socket)
        {
            temp = table_player.at(i);
            break;
        }
    }
    return temp;
}
// fonction qui va nous permettre de récupérer un joueur par son nom
// la fonction recoit en paramètre la liste des joueur et le nom(pseudo)
// et renvoit le joeur correspand de la liste des joueurs connecté au serveur
player_t get_player_by_name(vector<player_t>table_player, string pseudo)
{
    player_t temp;
    for(int i(0); i < table_player.size(); ++i)
    {
        if(table_player.at(i).player_pseudo == pseudo)
        {
            temp = table_player.at(i);
            break;
        }
    }
    return temp;
}
// fonction qui va mettre à jour les information du joueur qui vient de se connecter au serveur
// la fonction recoit en paramètre la liste des joueurs, le socket, le pseudo, l'adresse ip et le port du joueur concerné
// renvoit vrai si l'insertion à été un success
void set_player(vector<player_t>&table_player, int socket, string pseudo, string port, string ip)
{
    player_t joueur;
    joueur.player_ip = ip;
    joueur.player_port = stoi(port);
    joueur.player_pseudo = pseudo;
    joueur.player_socket = socket;
    joueur.player_modified = true;

    table_player.push_back(joueur);
}
// Fonction qui va afficher pour le serveur la liste des joueurs connectés
void afficher_player(vector<player_t>table_player)
{
    for(int i(0); i < table_player.size(); ++i)
    {
        cout << table_player.at(i).player_pseudo << "\t"
            << table_player.at(i).player_ip << "\t"
            << table_player.at(i).player_port << "\t";
        if(table_player.at(i).player_libre == true)
            cout << "libre\t";
        else
            cout << "busy\t";
        cout << endl;
    }
}
// Fonction qui va enlever le joueur de la liste des joueur connecté
// peut arrivé si le  joueur se déconnecte
void remove_player(vector<player_t>&table_player, player_t joueur)
{
    int position(0);
    for(int i(0); i < table_player.size(); ++i)
    {
        if(table_player.at(i).player_pseudo == joueur.player_pseudo)
        {
            position = i;
            break; // on le trouve et on quitte la boucle
        }
    }
    //ensuite à l'aide de la position on va chercher le vecteur puis l'effacer
    table_player.erase(table_player.begin()+position);
}
// methode pour envoyer un message à tout le monde dans liste
// afin de les avertir s'il y'a soit un nouveau joueur, soit un joueur est libre ou pas ou s'il est deconnecté
void message_players(vector<player_t> table_player, string cmd)
{ 
    vector<string> table_cmd_temp = split_cmd(cmd);
    string temp_cmd, pseudo, ip, sender;
    bool libre;
    int sender_socket(0), port;

    temp_cmd = table_cmd_temp[0];                 // on extrait la commande à envoyer
    temp_cmd.append(":");
    // Utilisé avec NUSR
    sender          = table_cmd_temp[1];
    //sender_socket   = get_player_name(sender).player_socket; //get_player_by_name
    sender_socket   = get_player_by_name(table_player, sender).player_socket;

    if( temp_cmd == "DUSR:" 
        ||temp_cmd == "SDWN:"
        ||temp_cmd == "EUSR:"
        ||temp_cmd == "BUSR:") // si quelqu'un s'est déconnecté ou le serveur
    {
        if(!table_player.empty())
        {
        for(int i(0); i < table_player.size(); i++)
        {
            sender_socket = table_player.at(i).player_socket;
            send_message(sender_socket, cmd);
        }
        }
    }
    //
    if(temp_cmd == "NUSR:")
    {
        for(int i(0); i < table_player.size(); i++)
        {
        pseudo = table_player.at(i).player_pseudo;
        ip     = table_player.at(i).player_ip;
        port   = table_player.at(i).player_port;
        libre  = table_player.at(i).player_libre;
        //
        temp_cmd.append(pseudo);
        temp_cmd.append(":");
        temp_cmd.append(ip);
        temp_cmd.append(":");
        temp_cmd.append(to_string(port));
        temp_cmd.append(":");
        temp_cmd.append(to_string(libre));
        // on envoit au user tout le monde
        send_message(sender_socket, temp_cmd);
        // on envoit à tout le monde le nouveau user
        if( pseudo != sender)
        {
            send_message(table_player.at(i).player_socket, cmd);
        }
        temp_cmd = "";
        temp_cmd ="NUSR:";
        }
    }
}
// fonction pour mettre à jour si le joueur est libre ou pas
// recoit en paramètre si la table des joueurs, le joueur à modifier ensuite son état
void set_busy(vector<player_t>&table_player, player_t joueur, bool est_libre_ou_non)
{
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == joueur.player_pseudo)
    {
      table_player.at(i).player_libre = est_libre_ou_non;
      break; // on le trouve et on quitte la boucle
    }
  }
}
//fonction qui permet de diviser une chaine de charactère en morceaux en fonction des éléments diviseurs
vector<string> split_cmd(string buffer)
{
  char *mots;
	char *context = NULL;
	char* char_line = (char*)buffer.c_str(); //on convertit du string en un tableau de caractères
	mots = strtok_r(char_line, ": -", &context);

	vector<string> cmd;//va contenir les expressions sans les espaces

	while(mots != NULL)
	{
		cmd.push_back(mots);//on entasse les expressions
		mots = strtok_r(NULL, ": -", &context);
	}
	return cmd;
}
//méthode pour envoyer un message vers un destination données
void  send_message(int socket, string message)
{
    char   out_buf[TAILLE_BUF]; 

    strcpy(out_buf, message.c_str());
    (void) send(socket, out_buf,  sizeof(out_buf), 0);
    // on vide le buffer après l'envoie
    for(int i(0); i < TAILLE_BUF; i++)
        out_buf[i] = '\0';
}