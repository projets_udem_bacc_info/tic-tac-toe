/**
 * 
 * fichier qui va contenir toutes  les fonctions de connection TCP /
 * client et serveur
*/
#include "tcp_srv_cnx_fns.hpp"

/*  
 *  connexion_tcp - Méthode qui se connecte à un service tcp specifique sur un host specifié
 * 
 *  @host     : nom du host vers laquelle la connexion veut se faire
 *  @service  : service associé à un port désiré
 * 
 * */
int connexion_tcp(const char *host, const char *service)
{
  return connexion_socket(host, service, "tcp");
}
/*  
 *  serveur_tcp - crée un sock en mode passif à utiliser dans un serveur tcp
 * 
 *  @service  : service associé à un port désiré
 * 
 * */
int serveur_tcp(const char *service, int qlen){
  return connexion_socket(service, "tcp", qlen);
}
/*  
 *  connexion_socket - Méthode générique qui alloue et connecte un socket en utilisant UPD(TCP)
 * 
 *  @qlen     : taille maximum de demande de connexion au serveur
 *  @service  : service associé à un port désiré
 *  @transport: le protocol à utiliser (upd ou tcp)
 * 
 * */
int connexion_socket(const char *service, const char *transport, int qlen )
{
  struct protoent     *ptr_proto;     // pointeur vers l'information sur le protocol à utiliser ici udp //ppe
  struct sockaddr_in  sin;            // structure pour extraire l'adresse ip internet du serveur
  socklen_t           s, type;        // description du socket et type du socket

  memset(&sin, 0, sizeof(sin));       // place les bytes d'une valeur donnée en block mémoire : 
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;

  // verification du port que l'application va utiliser
  if((sin.sin_port = htons((unsigned short)atoi(service))) == 0){
    erreur_exit("Info port non disponible", service);
  }
  // verification si le protocole utilisé est correcte, ici tcp
  if((ptr_proto = getprotobyname(transport)) == 0){
    erreur_exit("Impossible d'obtenir le protocole %s \n", transport);
  }
  // choix du choix de protocole de transport. Cette méthode peut être utilisé pour tcp comme udp
  if(strcmp(transport, "udp") == 0){
    type = SOCK_DGRAM;              // udp
  }
  else{
    type = SOCK_STREAM;             // tcp
  }
  // on alloue un socket
  s = socket(PF_INET, type, ptr_proto->p_proto);
  if(s < 0){
    erreur_exit("Impossible créer un socket : %s\n", strerror(errno));
  }
  // on lie le socket (ip au port)
  if(bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0){
    erreur_exit("Impossible de se lier au port : %s : %s\n", service, strerror(errno));
  }
  if(type == SOCK_STREAM && listen(s, qlen) < 0){
    erreur_exit("Impossible d'écouter sur le port : %s : %s\n", service, strerror(errno));
  }
  return s;
}
/*  
 *  connexion_socket - Méthode générique qui alloue et connecte un socket en utilisant UPD(TCP)
 * 
 *  @host     : nom du host vers laquelle la connexion veut se faire
 *  @service  : service associé à un port désiré
 *  @transport: le protocol à utiliser (upd ou tcp)
 * 
 * */
int connexion_socket(const char *host, const char *service, const char *transport )
{
  struct protoent     *ptr_proto;     // pointeur vers l'information sur le protocol à utiliser ici udp
  struct sockaddr_in  sin;            // structure pour extraire l'adresse ip internet du serveur
  int                 s, type;        // description du socket et type du socket

  memset(&sin, 0, sizeof(sin));       // place les bytes d'une valeur donnée en block mémoire : 
                                      //façon rapide de remplir des zéros dans une grand tableau ou une structure
  sin.sin_family = AF_INET;
  // verification du port que l'application va utiliser
  if((sin.sin_port = htons((unsigned short)atoi(service))) == 0){
    erreur_exit("Info port no disponible", service);
  }
  // verification de l'adresse ip telque recu en paramètre en format décimal
  if((sin.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE){
    erreur_exit("Impossible d'obtenir l'hote %s", host);
  }
  // verification si le protocole utilisé est correcte, ici upd
  if((ptr_proto = getprotobyname(transport)) == 0){
    erreur_exit("Impossible d'obtenir le protocole %s \n", transport);
  }
  // choix du choix de protocole de transport. Cette méthode peut être utilisé pour tcp comme udp
  if(strcmp(transport, "udp") == 0){
    type = SOCK_DGRAM;              // udp
  }
  else{
    type = SOCK_STREAM;             // tcp
  }
  // on alloue un socket
  s = socket(PF_INET, type, ptr_proto->p_proto);
  if(s < 0){
    erreur_exit("Impossible créer un socket : %s\n", strerror(errno));
  }
  // on connecte le socket
  if(connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0){
    erreur_exit("Impossible de se connecter au serveur %s.%s: %s\n", host, service, strerror(errno));
  }
  return s;
}
/*  
 *  erreur exit - affiche un message d'erreur and quitte le programme
 * 
 * */
int erreur_exit(const char *format, ...)
{
  va_list args;

  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  exit(1);
}