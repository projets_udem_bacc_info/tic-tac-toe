/**
 * NOM: MUKEYA KASSINDYE ARTHSON
 * LAB  6: FICHIER SERVEUR  TCP
 * serveur tcp en mode de connection parallelle 
 * 
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/signal.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <sys/errno.h>

#include <pthread.h>


#include <netinet/in.h>
#include <arpa/inet.h>
#include <algorithm>

#include <netdb.h>
#include <unistd.h>
#include <iostream> 
#include <cstdlib>
#include <string.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <stdexcept>
//include pour gestion d'erreur
#include <stdarg.h>
#include <stdio.h>
#include "ticmain.hpp"

using namespace std;

#define  PORT_NUM   "1155" 


#define  IP_ADDR   "127.0.0.1"//"192.168.2.20"
#define  PORT_NUM_c  "1055"

#define  TAILLE_BUF 4096

#define QLEN 32                         // nombre maximal des connexion en attente au serveur

#define INTERVAL    5
#define MAX_PLACE   3




//table des utilisateurs

struct player_t
{
  string player_pseudo;              // pseudo du client
  //char player_ip[INET_ADDRSTRLEN];     // contenir adress ip du client
  string player_ip;     // contenir adress ip du client
  int  player_port;                    // contenir le port du client
  int  player_socket;                  // socket du client pour communication
  bool player_libre = true;            // dire si le joeur est libre ou pas
  bool player_modified = false;          // dire s'il y a eu un changement d'état sur le joueur ou pas
};

struct info_t               // maitiendra les socket des toutes les connections
{
    int serveur_list;       // lorsque je me connecte au serveur de liste des joueurs
    int serveur_main;       // lorsque je me connecte en tant que serveur principale(écoute)
    int jeux_client;        // lorsque je me connecte à un client pour jouer
    int jeux_seveur;        // lorsque je recois la demande d'un client pour jouer
}t_socket_cnx = {0,0,0,0};

struct info_jeu_t
{
    string pseudo;
    bool init;
    bool agagner;
    bool placer;
    string pillon;
    int  n_fois_placer;
    int score;          // présent que pendant le jeux
}mon_jeux;
//structure qui va maintenir la demande de connexion à quelq'un
struct demande_cnx_t
{
    string from;
    string to;
    string answer;
}table_demande_de_cnx;

pthread_mutex_t   adresses_de_connection_mutex;
pthread_mutex_t   table_player_mutex;
vector<player_t>  table_player;

extern int        errno;                // gestion des erreurs

int erreur_exit(const char *format, ...);

int serveur_tcp(const char *service, int qlen);
int connexion_socket(const char *service, const char *transport, int qlen );
int echange_avec_client(void *); // illustrer échange entre client et serveur
int echange_moi_comme_client(void*);

int connexion_tcp(const char *host, const char *service);
int connexion_socket(const char *host, const char *service, const char *transport );

static void *sig_thread(void *arg);
void afficher_player(void);

player_t get_player_socket(int);
player_t get_player_name(string);
player_t get_player(string);
void set_player(int, string);
void remove_player(player_t);
void edit_player(player_t);
void set_busy(player_t);


vector<string> split_cmd(string);
void message_to_server(string);


int connection_au_serveur_liste(string, char *);// sera le port du serveur
void attente_connexion_client(void *);
int echange_avec_server(void* );
bool estpresent(string);
void menu_generale();
void menu_deconnection();

char   out_buf[TAILLE_BUF]; 
char   in_buf[TAILLE_BUF]; 

vector<vector<string>> grille(TAILLE_BOARD, vector<string>(TAILLE_BOARD));      // initialisation de la grille
pthread_mutex_t  grille_mutex;

coord_t coord;                  // va contenir les coordonnées entré par le joueur pour le jeux

// Main 
int main(int argc, char *argv[])
{
    struct sockaddr_in  fsin;                       // l'adresse provenant du client              
    socklen_t  t_addr;                                // t_addr : va contenir la taille adresse du client
    //int ms, ss;
    int client_s, serveur_s;                                     // ms : socket maitre, ss : socket esclave
    char *port;

    switch(argc)
    {
        case 1:
        break;
        case 2:
            port = argv[1];
        break;
        default:
            erreur_exit("usage : serv2 [port]\n");
    }

    //ms = serveur_tcp(PORT_NUM, QLEN);
    //
    serveur_s = serveur_tcp(port, QLEN);
    // je crée le thread d'écoute en attendant la connection d'un autre client

    pthread_t   th_signals, th_main, th_client_serv, th_serv_client;
    pthread_attr_t  th_ta;

    sigset_t set;
           int s;

    //(void)signal(SIGCHLD,tue_zombie);

    (void) pthread_attr_init(&th_ta);
    (void) pthread_attr_setdetachstate(&th_ta, PTHREAD_CREATE_DETACHED);
    (void) pthread_mutex_init(&table_player_mutex, 0);
    (void) pthread_mutex_init(&adresses_de_connection_mutex, 0);


    (void) pthread_mutex_lock(&adresses_de_connection_mutex);
    t_socket_cnx.serveur_main = serveur_s;
    (void) pthread_mutex_unlock(&adresses_de_connection_mutex);

    /* Block SIGQUIT and SIGUSR1; other threads created by main()
              will inherit a copy of the signal mask. */
    sigemptyset(&set);
    sigaddset(&set, SIGQUIT);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGUSR1);
    s = pthread_sigmask(SIG_BLOCK, &set, NULL);
    if (s != 0)
        erreur_exit("pthread_sigmask: %s\n", strerror(errno));

    if(pthread_create(&th_main, &th_ta, &sig_thread, (void *) &set) < 0)
    {
        erreur_exit("pthread_create(th_signals): %s\n", strerror(errno));
    }
    //
    string s_cmd, pseudo;
    cout << "pseudo >> : "; getline(cin, pseudo);
    // menu du jeux
    int socket_clt = connection_au_serveur_liste(pseudo, port); // connecction to server liste
    if(socket_clt > 0)
    {
        if(pthread_create(&th_client_serv, &th_ta, (void * (*)(void *))echange_avec_server, (void*)&socket_clt) < 0)
        {
            erreur_exit("pthread_create(th_main): %s\n", strerror(errno));
        }
    }
    // on enregistre dans notre variable globale chaque trace de connexion
    (void) pthread_mutex_lock(&adresses_de_connection_mutex);
    t_socket_cnx.serveur_list = socket_clt;
    (void) pthread_mutex_unlock(&adresses_de_connection_mutex);
    // starting the server and wait for connections
    if(pthread_create(&th_main, &th_ta, (void * (*)(void *))attente_connexion_client, (void*)&serveur_s) < 0)
    {
        erreur_exit("pthread_create(th_main): %s\n", strerror(errno));
    }
    string term_cmd, msg_to_srv, option;    // recevra les commandes du terminal
    vector<string> t_term_cmd;
    string client_ip_adress, client_port_number, out_coord, from_coord, to_coord;
    mon_jeux.n_fois_placer = 0;
    do
    {
        cout << "[@" << pseudo << "]> : ";
        getline(cin, term_cmd);

        t_term_cmd = split_cmd(term_cmd);

        if(t_term_cmd[0] == "HLP" )
        {
            menu_generale();
        }
        else if(t_term_cmd[0] == "LST")
        {
            (void) pthread_mutex_lock(&table_player_mutex);
            afficher_player();
            (void) pthread_mutex_unlock(&table_player_mutex);
        }
        else if(t_term_cmd[0] == "APLY")
        {
            if(t_term_cmd.size() == 2)
            {
                // peut-être enregistrer dans une table de message ceux qui nous demande de jouer
                msg_to_srv = t_term_cmd[0];
                msg_to_srv.append(":");
                player_t pl2 = get_player(t_term_cmd[1]);
                //cout << pl2.player_pseudo << endl;
                if(pl2.player_libre) // si l'autre joueur est libre
                {
                    // on doit se connecter en tant que client cette fois ci chez le joueur
                    cout << "veut jouer avec " << pl2.player_pseudo << endl;
                    client_ip_adress = pl2.player_ip; 
                    client_port_number = to_string(pl2.player_port);
                    // connection chez le joueur qui est serveur
                    client_s = connexion_tcp(client_ip_adress.c_str(), client_port_number.c_str());

                    if(client_s > 0)
                        cout << "connection avec " << pl2.player_pseudo << " établie. : " << client_s << endl;
                    else
                        erreur_exit("Impossible d'établir la connexion");
                    
                    //on enregistre dans notre variable globale chaque trace de connexion
                    (void) pthread_mutex_lock(&adresses_de_connection_mutex);
                    t_socket_cnx.jeux_client = client_s;
                    (void) pthread_mutex_unlock(&adresses_de_connection_mutex);
                    
                    //on envoie la demande de connection au joueur avec lequel on veut jouer
                    msg_to_srv.append(pl2.player_pseudo);
                    msg_to_srv.append(":");
                    msg_to_srv.append(pseudo);
                    msg_to_srv.append(":");
                    msg_to_srv.append(to_string(client_s));
                    set_player(client_s, pl2.player_pseudo);
                    strcpy(out_buf, msg_to_srv.c_str());
                    (void) send(client_s, out_buf, sizeof(out_buf), 0);
                    // on enregistre la demande de connection que l'on a effectué
                    table_demande_de_cnx.from   = pl2.player_pseudo;
                    table_demande_de_cnx.to     = pseudo;
                    table_demande_de_cnx.answer = "N/A"; 
                    // créons un thread pour gérer cette sous connection
                    // echange_moi_comme_client
                    // if(pthread_create(&th_serv_client, &th_ta, (void * (*)(void *))echange_moi_comme_client, (void*)&client_s) < 0)
                    // {
                    //     erreur_exit("pthread_create(th_main): %s\n", strerror(errno));
                    // }
                    if(pthread_create(&th_serv_client, &th_ta, (void * (*)(void *))echange_avec_client, (void*)&client_s) < 0)
                    {
                        erreur_exit("pthread_create(th_main): %s\n", strerror(errno));
                    }
                }
                else
                {
                    cout << pl2.player_pseudo << " est occupé(e)..." << endl;
                }
            }
            else
            {
                cout << "Utilisation : APLY \"nom du joueur\"" << endl;
            }
        }
        else if(t_term_cmd[0] == "RPLY")// on répond à celui qui nous a envoyé
        {
            if(t_term_cmd.size() == 3)
            {
                option = t_term_cmd[1];
                // si ma réponse est positive
                // je me mets en mode non libre
                // je mets à jour ma structure de jeux
                if(option == "O")
                {
                    // si je suis libre ou pas encore repondu
                    player_t from = get_player(pseudo);
                    player_t to = get_player(t_term_cmd[2]);
                    msg_to_srv.append(t_term_cmd[0]);  // [0]
                    msg_to_srv.append(":");
                    msg_to_srv.append(option);          // [1]
                    msg_to_srv.append(":");
                    msg_to_srv.append(t_term_cmd[2]);   // to // [2] on répond à celui qui demande
                    msg_to_srv.append(":");
                    msg_to_srv.append(pseudo);          // from : pour garder la trace // [3]

                    // si on accepte la connexion
                    // on passe en mode non libre
                    //edit_player(from);
                    message_to_server("EUSR:");
                    //
                    table_demande_de_cnx.from   = to.player_pseudo;
                    table_demande_de_cnx.to     = from.player_pseudo;
                    table_demande_de_cnx.answer = "YES";
                    mon_jeux.pseudo = from.player_pseudo;
                    strcpy(out_buf, msg_to_srv.c_str());
                    (void) send(to.player_socket, out_buf, sizeof(out_buf), 0);
                    msg_to_srv = "";
                }
            }
            else
            {
                cout << "RPLY - [O|N] @player" << endl;
            }
        }
        else if(t_term_cmd[0] == "PLC")
        {
            // est que le deuxième paramètre est présent (coord)
            if(t_term_cmd.size() == 2)
            {
                out_coord = t_term_cmd[1];
                // si on n'a pas encore placer
                // on converti les coordonnées
                // on les place sur la grille
                // on envoit le résulat à l'autre joueur
                // on affiche la grille
                if(mon_jeux.placer == false)
                {
                    coord_t coord = convertir_coordonnees(out_coord);
                    if(est_libre(grille, coord))
                    {
                        if(mon_jeux.n_fois_placer < MAX_PLACE)
                        {
                            grille = place_on_board(grille, mon_jeux.pillon, coord );
                            mon_jeux.n_fois_placer++;
                            // on les envoit au prochain joueur
                            //TIC:PLC:PILLON:COORD:FLAG
                            msg_to_srv.append("TIC:PLC:");
                            msg_to_srv.append(mon_jeux.pillon);
                            msg_to_srv.append(":");
                            msg_to_srv.append(out_coord);
                            msg_to_srv.append(":");
                            if(mon_jeux.n_fois_placer == 3)
                            {
                                if(a_gagne(grille))
                                {
                                    cout << "Vous avez gagné !" << endl;
                                    mon_jeux.score += 100;
                                    cout << "Votre score : " << mon_jeux.score << endl;
                                    // on réinitialise la grille
                                    msg_to_srv.append("1");
                                    init_board(grille);
                                    // méthode pour demander si on veut continuer
                                    mon_jeux.agagner = true;
                                }
                            }
                            msg_to_srv.append("0");
                            // msg_to_srv.append(":");
                            // msg_to_srv.append("0");
                            strcpy(out_buf, msg_to_srv.c_str());
                            // on obtient le joueur avec qui on joue
                            player_t joueur = get_player_name(table_demande_de_cnx.from);
                            //cout << "to be sent to : " << out_buf << table_demande_de_cnx.to << joueur.player_socket << endl;
                            (void) send(joueur.player_socket, out_buf, sizeof(out_buf), 0);
                            cout << endl;
                            show_board(grille);
                            mon_jeux.placer = true;
                            msg_to_srv = "";
                        }
                        else
                        {
                            cout << "Vous placez tous vos éléments, il est temps de jouer" <<endl;
                            cout << "Utilisez la commande PLY [DE] COORD [A] COORD" << endl;
                        }
                    }
                    else
                    {
                        cout << "Place occupé par un autre pillon" << endl;
                    }

                }
                else
                {
                    cout << "vous avez déjà placer votre pillon attendez votre tour." << endl;
                }
            }
            else
            {
                cout << "Commande incomplete... <PLC COORDONNEES>" << endl;
            }
            // on commence à placer les éléments sur la liste
        }
        else if(t_term_cmd[0] == "PLY")
        {
            if(t_term_cmd.size() == 3)
            {
                coord_t coord1, coord2;
                from_coord = t_term_cmd[1];
                to_coord   = t_term_cmd[2];
                // on convertit les coord
                coord1 = convertir_coordonnees(from_coord);
                coord2 = convertir_coordonnees(to_coord);
                // véfions dabord si la destination est libre
                // verifions si le pillon qu'on deplace est le notre
                // vérifions s'il a déjà fini de placer tous les pillons
                if(mon_jeux.placer == false)
                {
                    if(est_libre(grille, coord2 ))
                    {
                        if(grille[coord1.x][coord1.y] == mon_jeux.pillon)
                        {
                            if(mon_jeux.n_fois_placer >= MAX_PLACE)
                            {
                                if(play_on_board(grille, coord1, coord2))
                                {
                                    // on réussit à placer sur la grille
                                    cout << endl;
                                    show_board(grille);
                                    // construisons la commande à envoyer
                                    msg_to_srv = "PLY:";
                                    msg_to_srv.append(from_coord);
                                    msg_to_srv.append(":");
                                    msg_to_srv.append(to_coord);
                                    msg_to_srv.append(":");
                                    msg_to_srv.append("0");
                                    if(a_gagne(grille))
                                    {
                                        cout << "Vous avez gagné !" << endl;
                                        mon_jeux.score += 100;
                                        cout << "Votre score : " << mon_jeux.score << endl;
                                        // on réinitialise la grille
                                        init_board(grille);
                                        // méthode pour demander si on veut continuer
                                        mon_jeux.agagner = true;
                                    }
                                    strcpy(out_buf, msg_to_srv.c_str());
                                    // on obtient le joueur avec qui on joue
                                    player_t joueur = get_player_name(table_demande_de_cnx.from);
                                    //cout << "to be sent to : " << out_buf << table_demande_de_cnx.to << joueur.player_socket << endl;
                                    (void) send(joueur.player_socket, out_buf, sizeof(out_buf), 0);
                                    show_board(grille);
                                    mon_jeux.placer = true;
                                    msg_to_srv = "";
                                }
                                else
                                {
                                    cout << "Deplacement erroné"<< endl;
                                }
                            }
                            else
                            {
                                cout << "finissez de placer vos pillons avant de jouer" << endl;
                            }
                        }
                        else
                        {
                            cout << "Le pillon que vous deplacez n'est pas le votre" << endl;
                        }
                    }
                    else
                    {
                        cout << "l'endroit ou voulez placer votre pillon est occupé" << endl;
                    }
                }
                else
                {
                    cout << "Attendez votre tour pour jouer de nouveau" << endl;
                }
            }
            else
            {
                cout << "PLY à 2 Paramètres : Utilisation PLY COORD1 COORD2" << endl;
            }

        }
        else if(t_term_cmd[0] == "STP")
        {
            if(t_term_cmd[1] == "J")
            {
                cout << "fin du jeu" << endl;
                // on ferme le jeux avec le client / serveur
                // Si je suis client
                if(t_socket_cnx.jeux_client > 0)
                    close(t_socket_cnx.jeux_client);
                if(t_socket_cnx.jeux_seveur > 0)
                    close(t_socket_cnx.jeux_seveur);
                // et on garde l'écoute
            }
            if(t_term_cmd[1] == "S")
            {
                cout << "deconnection du serveur..." << endl;
                message_to_server("DUSR:");
                exit(EXIT_SUCCESS);
            }
        }
        else
        {
            cout << "Commande inconnue" << endl;
        }
        for(int i(0); i < TAILLE_BUF; ++i)
        {
            in_buf[i] = '\0'; // clear buffer
            out_buf[i]= '\0';
        }
        if(mon_jeux.agagner)
        {
            cout << "voulez-vous continuer? " << endl;
            cout << "Tapez HLP pour les commandes nécessaires" << endl;
        }
        msg_to_srv = "";
        //  cout << t_term_cmd[0] << endl;
        // t_term_cmd.clear();
        // term_cmd = "";
        // cin.clear();
 
    }while(1);
    close(serveur_s);
    //fin
    return 0;
}
//
player_t get_player(string player)
{
  player_t temp;
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == player)
      temp = table_player.at(i);
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
  return temp;
}
//
void menu_generale()
{
    cout << "HLP -\t[ G ]\t: Afficher le menu des commandes." << endl;
    cout << "HLP -\t[ J ]\t: Afficher le menu des commandes du jeu." << endl;
    cout << "LST -\t< pseudo du joueur >\t: Vérifie le status du joueur." << endl;
    cout << "LST -\t[ L ]ibre | [ O ]ccupé\t: Vérifie le status des joueurs." << endl;
    cout << "STP -\t< pseudo du joueur >\t: Arrêter la partie en avertissant le joueur." << endl;
    cout << "STP -\t[ S ]\t: Se deconnecter du serveur." << endl;
    cout << "STP -\t[ J ]\t: Fin du jeu." << endl;
    cout << "APLY -\t< pseudo du joueur >\t: Initier le début de la partie." << endl;
    cout << "RPLY -\t[ O ]ui | [ N ]on < pseudo du joueur >\t: Accepter ou refuser le début de la partie." << endl;
    cout << "PLC -\t<COORD>\t: Place votre pillon sur la grille" << endl;
    cout << "PLY -\t[DE]<COORD1> [A] <COORD2>\t : Deplaces votre pillon de COORD1 à COORD2" << endl;
    cout << "COORD : [H : Haut][M : Milieu][B : Bas]" << endl;
    cout << "COORD : [G : Gauche][C : Centre][D : Droite]" << endl;
    cout << "USAGE : [H|M|B][G|C|D] ex: HG : Haut à Gauche"<< endl;
}
//
void afficher_player()
{
  for(int i(0); i < table_player.size(); ++i)
  {
    cout << table_player.at(i).player_pseudo << "\t"
         << table_player.at(i).player_ip << "\t"
         << table_player.at(i).player_port << "\t"
         << table_player.at(i).player_socket << "\t";
    if(table_player.at(i).player_libre == true)
      cout << "libre\t";
    else
      cout << "busy\t";
    cout << endl;
  }
}
//
vector<string> split_cmd(string buffer)
{
  char *mots;
	char *context = NULL;
	char* char_line = (char*)buffer.c_str(); //on convertit du string en un tableau de caractères
	mots = strtok_r(char_line, ": -", &context);

	vector<string> cmd;//va contenir les expressions sans les espaces

	while(mots != NULL)
	{
		cmd.push_back(mots);//on entasse les expressions
		mots = strtok_r(NULL, ": -", &context);
	}
	return cmd;
}
// méthode qui renvoit le joueur demandé
player_t get_player_socket(int socket)
{
  player_t temp;
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_socket == socket)
      temp = table_player.at(i);
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
  return temp;
}
// méthode qui renvoit le joueur demandé
player_t get_player_name(string pseudo)
{
  player_t temp;
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == pseudo)
      temp = table_player.at(i);
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
  return temp;
}
//méthode pour modifier le nom du player
void set_player(int socket, string pseudo)
{
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == pseudo)
    {
      table_player.at(i).player_socket = socket;
      table_player.at(i).player_modified = true;
    }
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
}
// méthode pour enlever les users
void remove_player(player_t joueur)
{
  int trace(0);
  bool trouve = false;
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == joueur.player_pseudo)
    {
      trace = i;
      trouve = true;
      break; // on le trouve et on quitte la boucle
    }
  }
  //ensuite à l'aide de trace on va chercher le vecteur puis l'effacer
  if(trouve)
  {
    table_player.erase(table_player.begin()+trace);
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
}
//
//méthode pour mettre à jour si le joueur est libre ou pas
void edit_player(player_t joueur)
{
  //int trace(0);
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == joueur.player_pseudo)
    {
        // table_player.at(i).player_ip = joueur.player_ip;
        // table_player.at(i).player_port = joueur.player_port;
        table_player.at(i).player_libre = false;
        break; // on le trouve et on quitte la boucle
    }
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
}
//méthode pour mettre à jour si le joueur est libre ou pas
void set_busy(player_t joueur)
{
  //int trace(0);
  (void) pthread_mutex_lock(&table_player_mutex);
  for(int i(0); i < table_player.size(); ++i)
  {
    if(table_player.at(i).player_pseudo == joueur.player_pseudo)
    {
      table_player.at(i).player_libre = true;
      break; // on le trouve et on quitte la boucle
    }
  }
  (void) pthread_mutex_unlock(&table_player_mutex);
}
//message pour signaler au serveur qu'on est mort
void message_to_server(string cmd)
{

  string temp_cmd;
  vector<string> t_cmd = split_cmd(cmd);

  temp_cmd = t_cmd[0]; //CMD:
  temp_cmd.append(":");
  // on ajoute son nom, son adress pour vérification
  if(t_cmd[0] == "DUSR")
  {
        temp_cmd.append("0");
        temp_cmd.append(":");
        temp_cmd.append("0.0.0.0");
        temp_cmd.append(":");
        temp_cmd.append("0000");
        temp_cmd.append(":");
        temp_cmd.append("0");
        strcpy(out_buf, temp_cmd.c_str());
        // on commence par fermer la connection vers le serveur de la liste des joeurs
        (void) send(t_socket_cnx.serveur_list, out_buf, sizeof(out_buf), 0);
        temp_cmd = "";
        close(t_socket_cnx.serveur_list);
        // ensuite si quelqu'un est connecté avec nous le serveur lui avertira
        // ensuite on ferme notre écoute
        close(t_socket_cnx.serveur_main);
  }
  // on dit au serveur que l'on est occupé
  if(t_cmd[0] == "EUSR" || t_cmd[0] == "BUSR")
  {
    strcpy(out_buf, temp_cmd.c_str());
    // on commence par fermer la connection vers le serveur de la liste des joeurs
    (void) send(t_socket_cnx.serveur_list, out_buf, sizeof(out_buf), 0);
  }
  temp_cmd = "";
  for(int i(0); i < TAILLE_BUF; ++i)
  {
      out_buf[i] = '\0'; // clear buffer
  }
}
/*  
 *  serveur_tcp - crée un sock en mode passif à utiliser dans un serveur tcp
 * 
 *  @service  : service associé à un port désiré
 * 
 * */
int serveur_tcp(const char *service, int qlen){
  return connexion_socket(service, "tcp", qlen);
}
/*  
 *  connexion_socket - Méthode générique qui alloue et connecte un socket en utilisant UPD(TCP)
 * 
 *  @qlen     : taille maximum de demande de connexion au serveur
 *  @service  : service associé à un port désiré
 *  @transport: le protocol à utiliser (upd ou tcp)
 * 
 * */
int connexion_socket(const char *service, const char *transport, int qlen )
{
  struct protoent     *ptr_proto;     // pointeur vers l'information sur le protocol à utiliser ici udp //ppe
  struct sockaddr_in  sin;            // structure pour extraire l'adresse ip internet du serveur
  socklen_t           s, type;        // description du socket et type du socket

  memset(&sin, 0, sizeof(sin));       // place les bytes d'une valeur donnée en block mémoire : 
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;

  // verification du port que l'application va utiliser
  if((sin.sin_port = htons((unsigned short)atoi(service))) == 0){
    erreur_exit("Info port non disponible", service);
  }
  // verification si le protocole utilisé est correcte, ici tcp
  if((ptr_proto = getprotobyname(transport)) == 0){
    erreur_exit("Impossible d'obtenir le protocole %s \n", transport);
  }
  // choix du choix de protocole de transport. Cette méthode peut être utilisé pour tcp comme udp
  if(strcmp(transport, "udp") == 0){
    type = SOCK_DGRAM;              // udp
  }
  else{
    type = SOCK_STREAM;             // tcp
  }
  // on alloue un socket
  s = socket(PF_INET, type, ptr_proto->p_proto);
  if(s < 0){
    erreur_exit("Impossible créer un socket : %s\n", strerror(errno));
  }
  // on lie le socket (ip au port)
  if(bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0){
    erreur_exit("Impossible de se lier au port : %s : %s\n", service, strerror(errno));
  }
  if(type == SOCK_STREAM && listen(s, qlen) < 0){
    erreur_exit("Impossible d'écouter sur le port : %s : %s\n", service, strerror(errno));
  }
  return s;
}
/*  
 *  erreur exit - affiche un message d'erreur and quitte le programme
 * 
 * */
int erreur_exit(const char *format, ...)
{
  va_list args;

  va_start(args, format);
  vfprintf(stderr, format, args);
  va_end(args);
  exit(1);
}

// méthode d'interaction entre le client et le serveur
// echange_moi_comme_client
int echange_avec_client(void *s)
{
    int socket =  *((int *) s), n(0);
    //confirmation de connection
    string cmd, temp_out_buf, option, pseudo, from, clt_s, cmd_jeux; // demande de completion du pseudo
    int n_elt_sur_grille(0);
    string from_pillon;             // le caractère à placer sur la grille
    string from_coord, to_coord;          // les coordonnées venant de l'autre joueur
    vector<string> t_cmd;
    
    while((n = recv(socket, in_buf, sizeof(in_buf),0)) > 0)
    {
        if(n < 0){
                erreur_exit("recv: %s\n", strerror(errno));
        }
        cmd = in_buf; //
        t_cmd = split_cmd(cmd);

        // on fait une demande de jeux
        // le serveur va extraire le pseudo du joueur à qui la demande ait été addressée
        // et l'envoyer au joueur concerné
        // si je suis connecté en tant que client, c'est moi qui demande et recoit la réponse RPLY
        // if(t_cmd[0] ==  "APLY")
        // {
        //     pseudo = t_cmd[1];
        //     from   = t_cmd[2];                // celui qui envoit
        //     player_t player = get_player_name(pseudo);
        //     cout << "demande de jeu de " << get_player_name(from).player_pseudo << endl;
    
        // }
        if(t_cmd[0] == "RPLY" )
        {
            option = t_cmd[1];                          //  la reponse pour dire oui ou non la demande fut accepté
            pseudo = t_cmd[2];                          //  le pseudo de celui qui me demande
            from   = t_cmd[3];                          //  celui qui a initié la demande
            // on vérifie si la démande lui avait été adressé
            // est-ce que celui à qui j'ai envoyé(to) est celui de qui la demande vient?(pseudo)
            if(table_demande_de_cnx.to == pseudo)
            {
                if( option == "O")                          //  la réponse que je recois de l'autre joeur(OUI)
                {
                    player_t joueur = get_player_name(from);
                    if(!joueur.player_libre)
                    {
                        cout << endl;
                        cout << "désolé je suis déjà entrain de jouer" << endl;
                    }
                    else
                    {
                        cout << "je me mets en mode non libre..." << endl;
                        // edit_player(joueur);
                        message_to_server("EUSR:");
                        // temp_out_buf = "EUSR:";
                        // temp_out_buf.append(joueur.player_pseudo);
                        // message_to_server(temp_out_buf);
                        temp_out_buf = "";
                        // envoyer au serveur que je suis occupé
                        cout << "j'initialise ma grille" << endl;
                        grille = init_board(grille);
                        cout << endl;
                        show_board(grille);
                        mon_jeux.pseudo = joueur.player_pseudo;
                        mon_jeux.init   = true;
                        mon_jeux.pillon = 'X';
                        mon_jeux.placer = true;
                        // Je mets à jour ma structure de jeux
                        // Je génère mon pillon 1 2 3
                        // je lui envoit le feux fert pour qu'il commence
                        // TIC:INIT:X:0:0 :TIC pour dire qu'on initialise, INIT : commande initialiser, X : charactère choisit pour jouer
                        cmd_jeux = "TIC:INIT:";
                        cmd_jeux.append(mon_jeux.pillon);
                        cmd_jeux.append(":");
                        cmd_jeux.append("0");
                        cmd_jeux.append(":");
                        cmd_jeux.append("0");
                        //
                        strcpy(out_buf, cmd_jeux.c_str());
                        (void) send(socket, out_buf, sizeof(out_buf), 0);
                        // je dis que le jeux à été accepté
                        table_demande_de_cnx.answer = "YES";
                        cmd_jeux = "";
                    }
                }
                else if ( option == "N")
                {
                    cout << "Demande de jeux refusé";
                    table_demande_de_cnx.answer = "NO";
                }
            }
            else
            {
                // demande inconnue
                cmd_jeux = "TIC:INIT:";
                cmd_jeux.append("0");
                cmd_jeux.append(":");
                cmd_jeux.append("0");
                cmd_jeux.append(":");
                cmd_jeux.append("1");
                strcpy(out_buf, cmd_jeux.c_str());
                (void) send(socket, out_buf, sizeof(out_buf), 0);
                cmd_jeux = "";
            }
        }
        // ici on recoit la reprise de l'autre joueur
        // TIC veut dire qu'on place encore
        // le 2eme param aura la commande  PLC : placer,
        // le 3eme le pillon de l'adversaire
        // le 4eme les coordonnées ou les placer sur la grille
        // on maintiendra un compteur des éléments sur la grille afin de savoir la fin des placements et le début des jeux
        if(t_cmd[0] == "TIC" && t_cmd[1] == "PLC" && t_cmd[4] == "0")// La commande du jeux
        {
            if(t_cmd[4] == "0")
            {
                from_pillon = t_cmd[2];
                from_coord  = t_cmd[3];         // faut qu'ils soient convertis en i et j
                coord = convertir_coordonnees(from_coord);
                grille = place_on_board(grille, from_pillon, coord);
                cout << endl;
                show_board(grille);
                cout << "A nous de placer" << endl;
                mon_jeux.placer = false;
            }
            else
            {
                cout << "Vous avez perdu !" << endl;
                cout << "Score : 0" << endl;
                init_board(grille);
                cout << endl;
                show_board(grille);

                table_demande_de_cnx.from   = "";
                table_demande_de_cnx.to     = "";
                table_demande_de_cnx.answer = "N/A";
                message_to_server("BUSR:");

                mon_jeux.placer = true;
            }
            // place le nouvel élément sur la grille : console principale
            // on retourne la main au thread principal pour que l'adversaire joue à son tour 
        }
        //
        if(t_cmd[0] == "PLY")
        {
            coord_t coord1, coord2;
            from_coord = t_cmd[1]; // de telle place
            to_coord   = t_cmd[2]; // à telle place
            // on convertit les coord
            coord1 = convertir_coordonnees(from_coord);
            coord2 = convertir_coordonnees(to_coord);
            if(play_on_board(grille, coord1, coord2 ))
            {
                cout << endl;
                show_board(grille);
                mon_jeux.placer = false;
            }
            if(t_cmd[3] == "1")
            {
                cout << endl;
                show_board(grille);
                cout << "vous avez perdu..." << endl;
                cout << "Score : 0" << endl;
                init_board(grille);

                table_demande_de_cnx.from   = "";
                table_demande_de_cnx.to     = "";
                table_demande_de_cnx.answer = "N/A";
                message_to_server("BUSR:");
                mon_jeux.placer = true;
            }
        }
        for(int i(0); i < TAILLE_BUF; ++i)
        {
            in_buf[i] = '\0'; // clear buffer
        }
    }
    close(socket);
     return 0;
}
// méthode d'attente de connection avec le client
// ici j'agis en tant que serveur qui recoit une connection du client pour jouer
void attente_connexion_client(void *s)
{
    //cout << "ici" << endl;
    struct sockaddr_in  fsin;                       // l'adresse provenant du client              
    socklen_t  t_addr;                                // t_addr : va contenir la taille adresse du client
    //int ms, ss;
    int client_s, n(0);
    int serveur_s =  *((int *) s);  

    string cmd, temp_out_buf, option, pseudo, from, clt_s; // demande de completion du pseudo
    string in_coord, in_pillon, from_coord, to_coord;
    coord_t coord_a_conv;
    vector<string> t_cmd;

    while(1)
    {
        t_addr = sizeof(fsin);                              // taille de l'adresse
        client_s = accept(serveur_s, (struct sockaddr *)&fsin, &t_addr);
        // on enregistre ici le socket avec qui on va communiquer
        if( client_s < 0){
            if(errno == EINTR)   // permet d'intercepter le signal au cas ou le père n'est pas au courant de la terminaison du fils
                continue;
            erreur_exit("Acceptation de la connexion échoué %s", strerror(errno));      
        }
        //connecté en tant que serveur de jeux
        (void) pthread_mutex_lock(&adresses_de_connection_mutex);
        t_socket_cnx.jeux_seveur = client_s;
        (void) pthread_mutex_unlock(&adresses_de_connection_mutex);
        while((n = recv(client_s, in_buf, sizeof(in_buf),0)) > 0)
        {
            if(n < 0){
                    erreur_exit("recv: %s\n", strerror(errno));
            }
            cmd = in_buf; //
            t_cmd = split_cmd(cmd);
            // on fait une demande de jeux
            // le serveur va extraire le pseudo du joueur à qui la demande ait été addressée
            // et l'envoyer au joueur concerné
            if(t_cmd[0] ==  "APLY")
            {
                pseudo = t_cmd[1];                // celui vers qui on envoit la demande(serveur)
                from   = t_cmd[2];                // celui qui envoit(client)
                // on me notifie mais je repondrais que par ligne de commande principale
                cout << "demande de jeu de " << get_player_name(from).player_pseudo << endl;
                // si je suis libre ou pas
                set_player(client_s, from);
                // cout << "ici...";
            }
            // on recoit du client l'accusé de reception si j'ai dit oui pour jouer
            // if(t_cmd[4] == "0" && t_cmd[1] == "INIT" && t_cmd[0] == "TIC")
            if(t_cmd[0] == "TIC" && t_cmd[1] == "INIT" && t_cmd[4] == "0")
            {
                // le client est prêt à jouer
                // j'initialise aussi ma grille
                grille = init_board(grille);
                mon_jeux.init   = true;
                mon_jeux.pillon = '*';
                mon_jeux.placer = false;
                //on affiche la grille
                cout << endl;
                show_board(grille);
                // ensuite je commence à placer mon 1er pillon
                cout << "placons notre premier pillon" << endl;
                // en rendant la main au thread
            }
            // on recoit l'élément placé sur la grille du client
            // if(t_cmd[4] == "0" && t_cmd[1] == "PLC" && t_cmd[0] == "TIC")
            if(t_cmd[0] == "TIC" && t_cmd[1] == "PLC")
            {
                // l'élément recu est déjà vérifié
                if(t_cmd[4] == "0")
                {
                    in_pillon = t_cmd[2];
                    in_coord  = t_cmd[3];
                    coord_a_conv = convertir_coordonnees(in_coord);
                    grille = place_on_board(grille, in_pillon, coord_a_conv);
                    cout << endl;
                    show_board(grille);
                    cout << "A Nous de placer" << endl;
                    mon_jeux.placer = false;
                }
                else
                {
                    cout << "Vous avez perdu !" << endl;
                    cout << "Score : 0" << endl;
                    init_board(grille);
                    cout << endl;
                    show_board(grille);
                    table_demande_de_cnx.from   = "";
                    table_demande_de_cnx.to     = "";
                    table_demande_de_cnx.answer = "N/A";
                    // redire au serveur qu'on n'est plus libre
                    message_to_server("BUSR:");
                    mon_jeux.placer = true;
                }
                
            }
            //
            if(t_cmd[0] == "PLY")
            {
                coord_t coord1, coord2;
                from_coord = t_cmd[1]; // de telle place
                to_coord   = t_cmd[2]; // à telle place
                // on convertit les coord
                coord1 = convertir_coordonnees(from_coord);
                coord2 = convertir_coordonnees(to_coord);
                if(play_on_board(grille, coord1, coord2 ))
                {
                    cout << endl;
                    show_board(grille);
                    mon_jeux.placer = false;
                }
                if(t_cmd[3] == "1")
                {
                    cout << endl;
                    show_board(grille);
                    cout << "vous avez perdu..." << endl;
                    cout << "Score : 0" << endl;
                    init_board(grille);
                    message_to_server("BUSR:");
                    mon_jeux.placer = true;
                }
                // on vérfi s'il a gagné
            }
            for(int i(0); i < TAILLE_BUF; ++i)
            {
                in_buf[i] = '\0'; // clear buffer
            }
        }
        close(client_s);
    }
    
    //return 0;
}
//méthode qui va capter un signal et l'envoyer à tous ceux qui sont connectés
//
static void *sig_thread(void *arg)
{
    sigset_t *set = (sigset_t *)arg;
    int s, sig;
    int sender_socket(0), server_socket(0);

    // (void) pthread_mutex_lock(&adresses_de_connection_mutex);
    // sender_socket = adresses_de_connection.socket_serveur_liste;
    // server_socket = adresses_de_connection.socket_serveur_princ;
    // (void) pthread_mutex_unlock(&adresses_de_connection_mutex);

    while (1) {
        s = sigwait(set, &sig);
        if (s != 0)
            erreur_exit("sig: %s\n", strerror(errno));
        switch(sig)
        {
          case SIGINT:
          {//on envoi un message à tous le monde en leur disant que le serveur est mort
            cout << "client disconnecting..." << endl; 
            message_to_server("DUSR:");
          }
          break;
          case SIGQUIT:
          {//on envoi un message à tous le monde en leur disant que le serveur est mort
            cout << "client disconnecting..." << endl; 
            message_to_server("DUSR:");
          }
          break;
          case SIGUSR1:
          {//on envoi un message à tous le monde en leur disant que le serveur est mort
            cout << "client disconnecting..." << endl; 
            message_to_server("DUSR:");
          }
          break;
          default:
          {
            cout << "Erreur survenue, le serveur doit s'arrêter..." << endl; 
            message_to_server("DUSR:");
            //message_players("SDWN:0:0");
          }
          break;
        }
        close(server_socket);
        exit(EXIT_SUCCESS);
    }
}

/*  
 *  connexion_tcp - Méthode qui se connecte à un service upd specifique sur un host specifié
 * 
 *  @host     : nom du host vers laquelle la connexion veut se faire
 *  @service  : service associé à un port désiré
 * 
 * */
int connexion_tcp(const char *host, const char *service)
{
  return connexion_socket(host, service, "tcp");
}
/*  
 *  connexion_socket - Méthode générique qui alloue et connecte un socket en utilisant UPD(TCP)
 * 
 *  @host     : nom du host vers laquelle la connexion veut se faire
 *  @service  : service associé à un port désiré
 *  @transport: le protocol à utiliser (upd ou tcp)
 * 
 * */
int connexion_socket(const char *host, const char *service, const char *transport )
{
  struct protoent     *ptr_proto;     // pointeur vers l'information sur le protocol à utiliser ici udp
  struct sockaddr_in  sin;            // structure pour extraire l'adresse ip internet du serveur
  int                 s, type;        // description du socket et type du socket

  memset(&sin, 0, sizeof(sin));       // place les bytes d'une valeur donnée en block mémoire : 
                                      //façon rapide de remplir des zéros dans une grand tableau ou une structure
  sin.sin_family = AF_INET;
  // verification du port que l'application va utiliser
  if((sin.sin_port = htons((unsigned short)atoi(service))) == 0){
    erreur_exit("Info port no disponible", service);
  }
  // verification de l'adresse ip telque recu en paramètre en format décimal
  if((sin.sin_addr.s_addr = inet_addr(host)) == INADDR_NONE){
    erreur_exit("Impossible d'obtenir l'hote %s", host);
  }
  // verification si le protocole utilisé est correcte, ici upd
  if((ptr_proto = getprotobyname(transport)) == 0){
    erreur_exit("Impossible d'obtenir le protocole %s \n", transport);
  }
  // choix du choix de protocole de transport. Cette méthode peut être utilisé pour tcp comme udp
  if(strcmp(transport, "udp") == 0){
    type = SOCK_DGRAM;              // udp
  }
  else{
    type = SOCK_STREAM;             // tcp
  }
  // on alloue un socket
  s = socket(PF_INET, type, ptr_proto->p_proto);
  if(s < 0){
    erreur_exit("Impossible créer un socket : %s\n", strerror(errno));
  }
  // on connecte le socket
  if(connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0){
    erreur_exit("Impossible de se connecter au serveur %s.%s: %s\n", host, service, strerror(errno));
  }
  return s;
}

int connection_au_serveur_liste(string pseudo, char *port)
{
    int clt_srv_socket = connexion_tcp(IP_ADDR, PORT_NUM_c);                   // s = socket en mode passif(lient)
    string p = port;
    //cout << "socket :" << s << endl;
    if(clt_srv_socket > 0)
        cout << "connection établie au serveur... " << endl;
    else
        erreur_exit("Impossible d'obtenir le protocole d'établir la connexion");
    //si la connection réussie
    // on s'identifie au serveur
    // cout << "pseudo >> : "; getline(cin, pseudo);
    string s_cmd;
    s_cmd = "CUSR:";
    s_cmd.append(pseudo);
    s_cmd.append(":");
    s_cmd.append(p); // doit envoyer au serveur son num de port local
    //pseudo = s_cmd;
    strcpy(out_buf, s_cmd.c_str());
    (void) send(clt_srv_socket, out_buf, sizeof(out_buf), 0);
    // on crée un thread qui s'en occupera
    return clt_srv_socket;
}
// les échanges entre client - serveur des liste des joueurs
int echange_avec_server(void* s)
{
    int socket =  *((int *) s), n(0);

    string cmd, pseudo, s_cmd, temp_out_buf, option;
    vector<string> t_cmd;

    while((n = recv(socket, in_buf, sizeof(in_buf),0)) > 0)
    {
        if(n < 0){
                erreur_exit("recv: %s\n", strerror(errno));
        }
        cmd = in_buf;             // on copie simplement l'interieur du buffer(char[]) dans la variable commande(string)
        t_cmd = split_cmd(cmd);   // on décompose la commande en option
        // notification du nouveau joueur
        if(t_cmd[0] == "NUSR")
        {
            // on met à jour la table de users
            player_t player;
            // if(! estpresent(t_cmd[1]))      // si le joueur est déjà présent, on évite une rédondance
            // {
                player.player_pseudo = t_cmd[1];
                player.player_ip = t_cmd[2];
                player.player_port = atoi(t_cmd[3].c_str());
                player.player_libre = (bool)(t_cmd[4].c_str());
                //
                (void) pthread_mutex_lock(&table_player_mutex);
                table_player.push_back(player);
                (void) pthread_mutex_unlock(&table_player_mutex);
                // cout << endl;
                cout << player.player_pseudo << " vient de se connecter..." << endl;
            // }
        }
        // notification de déconnection d'un joueur ou du serveur
        if(t_cmd[0] == "DUSR")
        {
            player_t player;
            player.player_pseudo = t_cmd[1]; // on specifie le pseudo du joueur
            cout << player.player_pseudo << " vient de se deconnecter..." << endl;
            remove_player(player);
        }
        if(t_cmd[0] == "EUSR")    // EUSER : EDIT USER : commande pour signaler un changement à l'état d'un jouer(libre ou pas)
        {                         // Intervient lorsque l'utilisateur est entrain de jouer avec un autre utilisateur
            player_t player;
            player.player_pseudo = t_cmd[1];      // pseudo du joueur à modifier
            // on met à jour le joueur concerné
            edit_player(player);
        }
        if(t_cmd[0] == "BUSR")    // EUSER : EDIT USER : commande pour signaler un changement à l'état d'un jouer(libre ou pas)
        {                         // Intervient lorsque l'utilisateur est entrain de jouer avec un autre utilisateur
            player_t player;
            player.player_pseudo = t_cmd[1];      // pseudo du joueur à modifier
            // on met à jour le joueur concerné
            set_busy(player);
        }
        if(t_cmd[0] == "SDWN")
        {
            erreur_exit("le serveur s'est arrêté... : %s\n", strerror(errno));
        }
        for(int i(0); i < TAILLE_BUF; ++i)
        {
            in_buf[i] = '\0'; // clear buffer
        }
        //t_cmd = "";
        s_cmd = "";
        cmd = "";
    }
    close(socket);
    return 0;
}
// verifie si le user est deja dans la table
bool estpresent(string pseudo)
{
	bool est_present = false;
	for(int i(0); i < table_player.size(); i++)
	{
		if(table_player.at(i).player_pseudo == pseudo)
		{
			est_present = true;
			break;
		}
	}
	return est_present;
}
//menu_deconnection
void menu_deconnection()
{

}