#ifndef TICMAIN_H
#define TICMAIN_H

#include <iostream>
#include <cstdlib>
#include <vector>

#define CRX 1       // modèle des pillons
#define PLS 2
#define SQR 3

#define TAILLE_BOARD    3   // taille de la planche de jeux

#define H   0               // HAUT
#define M   1               // MILIEU
#define B   2               // BAS
#define D   2               // DROITE
#define C   1               // CENTRE
#define G   0               // GAUCHE

using namespace std;

struct coord_t
{
    int x;
    int y;
    bool valide;
};

vector<vector<string>> init_board(vector<vector<string>> );          // fonction qui va initialiser la planche de jeux
void show_board(vector<vector<string>>);          // affichage de la planche de jeux
vector<vector<string>> place_on_board(vector<vector<string>>, string, coord_t);  // place les charactère sur la table
bool play_on_board(vector<vector<string>>&, coord_t, coord_t);
bool est_libre(vector<vector<string>>, coord_t);             // on vérifie si le carreau est libre ou pas avant de placer
bool a_gagne(vector<vector<string>>);             // vérifie si quelqu'un a gagné
int  n_elts_sur_la_grille(vector<vector<string>>);            // renvoit le nombre d'éléments déjà placé sur la grille
coord_t convertir_coordonnees(string coord);    
bool deplacement_valide(coord_t, coord_t);            // renvoit les coordonnées en format x et y et si s'il sont valides

#endif