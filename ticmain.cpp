// fichier qui contiendra les fichiers
#include "ticmain.hpp"

vector<vector<string>> init_board(vector<vector<string>> grille)          // fonction qui va initialiser la planche de jeux
{
    for(int i(0); i < TAILLE_BOARD; i++)
    {
        for(int j(0); j < TAILLE_BOARD; j++)
        {
            grille[i][j] = "0";
        }
    }
    return grille;
}

void show_board(vector<vector<string>> grille)          // affichage de la planche de jeux
{
    for(int i(0); i < TAILLE_BOARD; i++)
    {
        for(int j(0); j < TAILLE_BOARD; j++)
        {
            cout << grille[i][j] << "\t";
        }
        cout << endl;
    }
    cout <<endl;
}
//
vector<vector<string>> place_on_board(vector<vector<string>> grille, string pillon, coord_t coord)  // place les charactère sur la table
{
    grille[coord.x][coord.y]  = pillon;
    return grille;
}
//
bool est_libre(vector<vector<string>> grille, coord_t coord)
{
    return grille[coord.x][coord.y] == "0" ? true:false;
}
//
int  n_elts_sur_la_grille(vector<vector<string>>grille)
{
    int compteur(0);
    for(int i(0); i < TAILLE_BOARD; i++)
    {
        for(int j(0); j < TAILLE_BOARD; j++)
        {
            if(grille[i][j] != "0")
                compteur++;
        }
    }
    return compteur;
}
//
coord_t convertir_coordonnees(string coord)
{
    coord_t temp;
    temp.x = -1;
    temp.y = -1;
    temp.valide = false;

    if(coord.length() == 2)
    {
        // coordonnées des lignes
        switch(coord.at(0))
        {
            case 'H':
                temp.x = H;
            break;
            case 'M':
                temp.x = M;
            break;
            case 'B':
                temp.x = B;
            break;
            default:
                temp.valide = false;
            break;
        }
        // coordonnées des colonnes
        switch(coord.at(1))
        {
            case 'G':
                temp.y = G;
            break;
            case 'C':
                temp.y = C;
            break;
            case 'D':
                temp.y = D;
            break;
            default:
                temp.valide = false;
            break;
        }
    }
    return temp;
}
//
bool play_on_board(vector<vector<string>> &grille, coord_t from, coord_t to)
{
    bool bien_place = false;
    // pour deplacer un un pillon vers un autre
    // on vérifie s'il se déplace correctemenet
    // on retourne vrai si l'élément est placé correctement
    if(deplacement_valide(from, to))
    {
        grille[to.x][to.y] = grille[from.x][from.y];
        grille[from.x][from.y] = "0";
        bien_place = true;
    }
    return bien_place;
}
// ON SE DEPLACE UNE CASE À LA FOIS
bool deplacement_valide(coord_t coord1, coord_t coord2)
{
    bool valide = false;
    if(coord1.x == H && coord1.y == G)
    {
        if((coord2.x == H && coord2.y == C)
         || (coord2.x == M && coord2.y == C)
         || (coord2.x == M && coord2.y == G))
         {
             valide = true;
         }
    }
    if(coord1.x == H && coord1.y == C)
    {
        if( (coord2.x == H && coord2.y == G)
         || (coord2.x == M && coord2.y == C)
         || (coord2.x == H && coord2.y == D))
         {
             valide = true;
         }
    }
    if(coord1.x == H && coord1.y == D)
    {
        if( (coord2.x == H && coord2.y == C)
         || (coord2.x == M && coord2.y == D)
         || (coord2.x == M && coord2.y == C))
         {
             valide = true;
         }
    }
    if(coord1.x == M && coord1.y == G)
    {
        if( (coord2.x == H && coord2.y == G)
         || (coord2.x == M && coord2.y == C)
         || (coord2.x == B && coord2.y == G))
         {
             valide = true;
         }
    }
    if(coord1.x == M && coord1.y == C)
    {
        if( (coord2.x == H && coord2.y == G)
         || (coord2.x == M && coord2.y == G)
         || (coord2.x == B && coord2.y == G)
         || (coord2.x == H && coord2.y == C)
         || (coord2.x == B && coord2.y == C)
         || (coord2.x == H && coord2.y == D)
         || (coord2.x == M && coord2.y == D)
         || (coord2.x == B && coord2.y == D))
         {
             valide = true;
         }
    }
    if(coord1.x == M && coord1.y == D)
    {
        if( (coord2.x == H && coord2.y == D)
         || (coord2.x == M && coord2.y == C)
         || (coord2.x == B && coord2.y == D))
         {
             valide = true;
         }
    }
    if(coord1.x == B && coord1.y == G)
    {
        if( (coord2.x == M && coord2.y == G)
         || (coord2.x == M && coord2.y == C)
         || (coord2.x == B && coord2.y == C))
         {
             valide = true;
         }
    }
    if(coord1.x == B && coord1.y == C)
    {
        if( (coord2.x == M && coord2.y == C)
         || (coord2.x == B && coord2.y == G)
         || (coord2.x == B && coord2.y == D))
         {
             valide = true;
         }
    }
    if(coord1.x == B && coord1.y == D)
    {
        if( (coord2.x == M && coord2.y == D)
         || (coord2.x == B && coord2.y == C)
         || (coord2.x == M && coord2.y == C))
         {
             valide = true;
         }
    }
    return valide;
}
//
bool a_gagne(vector<vector<string>> grille)
{
    bool agagne = false;
    //
    if((grille[H][G] == grille[H][C]) && (grille[H][G]== grille[H][D]))
    {
        if( grille[H][G] != "0" && grille[H][C] != "0" && grille[H][D] != "0" )
            agagne = true;
    }
    if((grille[M][G] == grille[M][C]) && (grille[M][G]== grille[M][D]))
    {
        if(grille[M][G] != "0" &&  grille[M][C] != "0" && grille[M][D] != "0")
            agagne = true;
    }
    if((grille[B][G] == grille[B][C]) && (grille[B][G]== grille[B][D]))
    {
        if(grille[B][G] != "0" && grille[B][C] != "0" && grille[B][D] != "0")
            agagne = true;
    }
    if((grille[H][G] == grille[M][G]) && (grille[H][G]== grille[B][G]))
    {
        if(grille[H][G] != "0" && grille[M][G] != "0" && grille[B][G] != "0" )
            agagne = true;
    }   
    if((grille[H][C] == grille[M][C]) && (grille[H][C]== grille[B][C]))
    {
        if(grille[H][C] != "0" && grille[M][C] != "0" && grille[B][C] != "0" )
            agagne = true;
    }    
    if((grille[H][D] == grille[M][D]) && (grille[H][D]== grille[B][D]))
    {
        if(grille[H][D] != "0" && grille[M][D] != "0" && grille[B][D] != "0")
            agagne = true;
    }
    if((grille[H][G] == grille[M][C]) && (grille[H][G]== grille[B][D]))
    {
        if(grille[H][G] != "0" && grille[M][C] != "0" && grille[B][D] != "0" )
            agagne = true;
    } 
    if((grille[H][D] == grille[M][C]) && (grille[H][D]== grille[B][G]))
    {
        if(grille[H][D] != "0" && grille[M][C] != "0" && grille[B][G] != "0")
            agagne = true;
    }

    return agagne;
}
